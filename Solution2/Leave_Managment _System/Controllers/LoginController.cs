﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Leave_Managment__System.Controllers;
 using Leave_Managment__System.Models;
using System.Web.Security;

namespace Leave_Managment__System.Controllers
{
 
    public class LoginController : Controller
    {
        // GET: Login
        sunbeamdbEntities dbObj = new sunbeamdbEntities();
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(EmployeeDetail loginObject, string ReturnUrl)
        {
            var MatchCount = (from e in dbObj.EmployeeDetails.ToList()
                              where e.username_.ToLower() == loginObject.username_.ToLower()
                              && e.password == loginObject.password
                              select e).ToList().Count();

            if (MatchCount == 1)
            {
               
                //Create / send cookie to client 
                FormsAuthentication.SetAuthCookie(loginObject.username_, false);

               if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "User Name or Password is incorrect";
                return View();
            }

        }

        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}